
Downstream
==========

https://bitbucket.org/wdzierzanowski/downstream-android

Downstream is an Android application offering efficient transmission of audio
files based on a combination of SSH-2 and GStreamer.  It uses an embedded SSH-2
client to connect to a host running a SSH-2 server and the GStreamer framework.
Some basic shell commands and `gst-launch' are executed remotely over the SSH-2
protocol.  The pipeline given to `gst-launch' is specially crafted so that the
remote host starts sending audio data via TCP.

Downstream uses the downstream-core library [1] as its backend.  If you are
thinking about contributing to Downstream or downstream-core, you might also be
interested in downstream-cli [2], which is a command-line client that can be
used to test the library without the overhead of using an Android device or
emulator.

Please read the included LICENCE.txt.

[1] https://bitbucket.org/wdzierzanowski/downstream-core
[2] https://bitbucket.org/wdzierzanowski/downstream-cli
