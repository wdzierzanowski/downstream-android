/*
 * Copyright (C) 2011-2012 Wojciech Dzierżanowski
 * See LICENSE.txt for licensing details.
 */

package wdzierzan.downstream.android.servermanager;

/**
 * @author Wojciech Dzierżanowski (wojciech.dzierzanowski@gmail.com)
 */
public interface Server {
    int getId();
    String getName();
    String getHostname();
    String getUsername();
    String getMusicPath();
}
