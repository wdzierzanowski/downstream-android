/*
 * Copyright (C) 2012 Wojciech Dzierżanowski
 * See LICENSE.txt for licensing details.
 */

package wdzierzan.downstream.android;

/**
 * @author Wojciech Dzierżanowski <wojciech.dzierzanowski@gmail.com>
 */
public enum Format {
    OGG_VORBIS("Ogg Vorbis", ".ogg"),
    MP3("MP3", ".mp3");

    private final String string;
    public final String extension;
    private Format(String string, String extension) {
        this.string = string;
        this.extension = extension;
    }
    @Override
    public String toString() {
        return string;
    }

    public static String[] strings() {
        Format[] v = values();
        String[] n = new String[v.length];
        for (int i = 0; i < v.length; i++) {
            n[i] = v[i].toString();
        }
        return n;
    }

    public static String[] names() {
        Format[] v = values();
        String[] n = new String[v.length];
        for (int i = 0; i < v.length; i++) {
            n[i] = v[i].name();
        }
        return n;
    }
}
